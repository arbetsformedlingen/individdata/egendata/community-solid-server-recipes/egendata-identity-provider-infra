# Service account to run the digital wallet.
apiVersion: v1
kind: ServiceAccount
metadata:
  name: egendata-identity-provider
---
# Service to expose and loadbalance between pods.
apiVersion: v1
kind: Service
metadata:
  name: egendata-identity-provider
spec:
  selector:
    app: egendata-identity-provider
  ports:
    - protocol: TCP
      port: 3000
      targetPort: 3000
---
# Deployment to create necessary pods with configurations.
apiVersion: apps/v1
kind: Deployment
metadata:
  name: egendata-identity-provider
  labels:
    app: egendata-identity-provider
spec:
  replicas: 1
  selector:
    matchLabels:
      app: egendata-identity-provider
  template:
    metadata:
      labels:
        app: egendata-identity-provider
    spec:
      # Pod to launch the service.
      serviceAccountName: egendata-identity-provider
      securityContext:
          # Your app shall always run as nonroot.
          runAsNonRoot: true
      affinity:
        # Avoid to run many pods on same physical node. It increases reliability.
        podAntiAffinity:
          preferredDuringSchedulingIgnoredDuringExecution:
          - weight: 1
            podAffinityTerm:
              labelSelector:
                matchLabels:
                  app: egendata-identity-provider
              topologyKey: kubernetes.io/hostname
      containers:
      - name: egendata-identity-provider
        image: docker-images.jobtechdev.se/egendata-identity-provider/egendata-identity-provider:latest
        imagePullPolicy: Always
        ports:
        - containerPort: 3000
        securityContext:
          # Your app shall always have the container file system read only. Use volumes for temp areas etc.
          readOnlyRootFilesystem: true
        resources:
          limits:
            memory: "1Gi"
          requests:
            memory: "1Gi"
        env:
          - name: CSS_CLIENT_ID
            valueFrom:
              secretKeyRef:
                name: egendata-identity-provider-secret
                key: idp-client-id
          - name: CSS_CLIENT_SECRET
            valueFrom:
              secretKeyRef:
                name: egendata-identity-provider-secret
                key: idp-client-secret
          - name: CSS_ADMIN_ACCESS_KEY
            valueFrom:
              secretKeyRef:
                name: egendata-identity-provider-secret
                key: admin-access-key
          - name: CSS_VISMA_CUSTOMER_KEY
            valueFrom:
              secretKeyRef:
                name: egendata-identity-provider-secret
                key: visma-customer-key
          - name: CSS_VISMA_SERVICE_KEY
            valueFrom:
              secretKeyRef:
                name: egendata-identity-provider-secret
                key: visma-service-key
        envFrom:
          - configMapRef:
              name: egendata-identity-provider-config
        volumeMounts:
        - name: npm
          mountPath: /.npm
        - name: tmp
          mountPath: /tmp
        - name: egendata-vol
          mountPath: /mnt/store
      volumes:
      - name: npm
        emptyDir: {}
      - name: tmp
        emptyDir: {}
      - name: egendata-vol
        persistentVolumeClaim:
          claimName: egendata-identity-provider-claim
